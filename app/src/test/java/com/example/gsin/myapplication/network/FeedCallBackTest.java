package com.example.gsin.myapplication.network;

import com.example.gsin.myapplication.CustomRobolectricRunner;
import com.example.gsin.myapplication.fake.RecordingEventBus;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;

import retrofit.Response;

@RunWith(CustomRobolectricRunner.class)
public class FeedCallBackTest {

    private FeedCallBack mFeedCallBack;

    private Response mResponse;

    private RecordingEventBus mRecordingEventBus = new RecordingEventBus();

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        mFeedCallBack = new FeedCallBack(mRecordingEventBus);
    }

    @Test
    public void successTest(){

        mFeedCallBack.onResponse(Response.success(null));

        Object lastPostedEvent = mRecordingEventBus.getLastPostedEvent();

        assert (lastPostedEvent != null);
    }

    @Test
    public void failTest(){

        mFeedCallBack.onFailure(new Throwable());

        Object lastPostedEvent = mRecordingEventBus.getLastPostedEvent();

        assert (lastPostedEvent != null);
    }
}
