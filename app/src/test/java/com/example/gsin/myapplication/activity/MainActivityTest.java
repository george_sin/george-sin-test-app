package com.example.gsin.myapplication.activity;

import android.widget.TextView;

import com.example.gsin.myapplication.CustomRobolectricRunner;
import com.example.gsin.myapplication.MainActivity;
import com.example.gsin.myapplication.R;
import com.example.gsin.myapplication.event.ListErrorEvent;
import com.example.gsin.myapplication.event.ListReceivedEvent;
import com.example.gsin.myapplication.model.ServerFeed;
import com.example.gsin.myapplication.network.NetworkManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.shadows.CoreShadowsAdapter;
import org.robolectric.util.ActivityController;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

@RunWith(CustomRobolectricRunner.class)
public class MainActivityTest {

    private ActivityController<MainActivity> mActivityController;
    private MainActivity mActivity;

    @Mock
    private NetworkManager networkManager;

    @Mock
    private ServerFeed mServerFeed;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mActivity = new MainActivity(networkManager);
        mActivityController = ActivityController.of(new CoreShadowsAdapter(), mActivity);
    }

    @Test
    public void successTestWithOutContent(){

        mActivityController.create();

        mActivity.onEventMainThread(new ListReceivedEvent(mServerFeed));

        TextView textview = (TextView) mActivity.findViewById(R.id.textview);

        assertNotEquals("Empty", textview.getText().toString());
    }


    @Test
    public void failTest(){

        mActivityController.create();

        mActivity.onEventMainThread(new ListErrorEvent());

        TextView textview = (TextView) mActivity.findViewById(R.id.textview);

        assertEquals("Empty", textview.getText().toString());
    }
}
