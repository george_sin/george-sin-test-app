package com.example.gsin.myapplication.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServerAlbumList {

    @SerializedName("entry")
    private List<ServerEntry> mEntries;

    public  List<ServerEntry> getEntries() {

        return mEntries;
    }
}
