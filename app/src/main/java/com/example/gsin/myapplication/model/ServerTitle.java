package com.example.gsin.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class ServerTitle {

    @SerializedName("label")
    private String mLabel;

    public String getLabel(){
        return mLabel;
    }
}
