package com.example.gsin.myapplication.module;

import android.app.Application;
import android.content.res.Resources;

public class ApplicationModule {

    private static Application sApplication;

    public static void setApplication(Application application) {
        sApplication = application;
    }

    public static Resources resources(){
        return sApplication.getResources();
    }
}
