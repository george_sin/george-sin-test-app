package com.example.gsin.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class ServerEntry {

    @SerializedName("title")
    private ServerTitle mServerTitle;

    public ServerTitle getTitle() {
        return mServerTitle;
    }

}
