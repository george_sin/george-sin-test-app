package com.example.gsin.myapplication.module;

import de.greenrobot.event.EventBus;

public class EventBusModule {

    private static EventBus sEventBus = EventBus.builder().throwSubscriberException(true).build();

    public static EventBus eventBus(){
        return sEventBus;
    }
}