package com.example.gsin.myapplication.API;

import com.example.gsin.myapplication.model.ServerFeed;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

public interface TopAlbums {
        @GET("gb/rss/topalbums/limit={number}/json")
        Call<ServerFeed> getTopAlbums(@Path("number") int number);
}
