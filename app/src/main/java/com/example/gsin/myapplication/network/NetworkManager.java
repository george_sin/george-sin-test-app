package com.example.gsin.myapplication.network;

import com.example.gsin.myapplication.API.TopAlbums;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class NetworkManager {

    private static final int NUMBER_OF_RECORDS = 10;
    private static final int TIMEOUT_PERIOD = 30;
    private static final String BASE_URL = "https://itunes.apple.com/";

    private Retrofit mRetrofit;
    private FeedCallBack mFeedCallBack;

    public NetworkManager(){
        this(new FeedCallBack());
    }

    public NetworkManager(FeedCallBack feedCallBack){
        mFeedCallBack = feedCallBack;
    }

    public void requestListOfAlbums() {

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(TIMEOUT_PERIOD, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(TIMEOUT_PERIOD, TimeUnit.SECONDS);

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        TopAlbums topAlbums = mRetrofit.create(TopAlbums.class);

        Call call = topAlbums.getTopAlbums(NUMBER_OF_RECORDS);

        call.enqueue(mFeedCallBack);
    }
}
