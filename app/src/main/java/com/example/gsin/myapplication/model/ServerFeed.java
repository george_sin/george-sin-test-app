package com.example.gsin.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class ServerFeed {

    @SerializedName("feed")
    private ServerAlbumList mServerAlbumList;

    public ServerAlbumList getFeed() {
        return mServerAlbumList;
    }
}
