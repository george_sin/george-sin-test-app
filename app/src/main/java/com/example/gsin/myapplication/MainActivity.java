package com.example.gsin.myapplication;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gsin.myapplication.event.ListErrorEvent;
import com.example.gsin.myapplication.event.ListReceivedEvent;
import com.example.gsin.myapplication.model.ServerAlbumList;
import com.example.gsin.myapplication.model.ServerEntry;
import com.example.gsin.myapplication.model.ServerFeed;
import com.example.gsin.myapplication.module.EventBusModule;
import com.example.gsin.myapplication.network.NetworkManager;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.fab)
    FloatingActionButton mFab;

    @Bind(R.id.progressspinner)
    ProgressBar mProgressBar;

    @Bind(R.id.textview)
    TextView mTextView;

    @Bind(R.id.contentlayout)
    RelativeLayout mRelativeLayout;

    NetworkManager mNetworkManager;
    protected EventBus mEventBus;

    public MainActivity(){
        this(new NetworkManager());
    }

    public MainActivity(NetworkManager networkManager){
        mNetworkManager = networkManager;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventBus eventBus = EventBusModule.eventBus();
        eventBus.registerSticky(this);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
    }

    @OnClick(R.id.fab)
    public void logInButtonClicked() {
        mProgressBar.setVisibility(View.VISIBLE);
        mNetworkManager.requestListOfAlbums();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onEventMainThread(ListReceivedEvent event) {

        Snackbar.make(mRelativeLayout, getString(R.string.success_message), Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();

        mProgressBar.setVisibility(View.INVISIBLE);

        mTextView.setText(getTopTen(event.getServerFeed()));
    }

    public String getTopTen(ServerFeed serverFeed){

        String listToShow = "";

        try {
            if (serverFeed != null) {

                ServerAlbumList serverList = serverFeed.getFeed();

                List<ServerEntry> list = serverList.getEntries();

                for (int i = 0; i < list.size(); i++) {

                    String position = Integer.toString(i+1) + ") ";

                    String label = position + list.get(i).getTitle().getLabel();

                    listToShow = listToShow + label + "\n\n";
                }
            }
        }
        catch(Exception e){
            Log.d("MainActivity", e.getMessage());
        }

        return listToShow;
    }

    public void onEventMainThread(ListErrorEvent event) {

        Snackbar.make(mRelativeLayout, getString(R.string.failed_message), Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();

        mProgressBar.setVisibility(View.INVISIBLE);

        mTextView.setText(getString(R.string.default_text));
    }

    @Override
    protected void onDestroy() {
        if(mEventBus!=null) {
            mEventBus.unregister(this);
        }
        super.onDestroy();
    }
}
