package com.example.gsin.myapplication.network;

import com.example.gsin.myapplication.event.ListErrorEvent;
import com.example.gsin.myapplication.event.ListReceivedEvent;
import com.example.gsin.myapplication.model.ServerFeed;
import com.example.gsin.myapplication.module.EventBusModule;

import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.Response;

public class FeedCallBack implements Callback {

    private EventBus mEventBus;

    public FeedCallBack(){
        this(EventBusModule.eventBus());
    }

    public FeedCallBack(EventBus eventBus){
        mEventBus = eventBus;
    }

    @Override
    public void onResponse(Response response) {

        ServerFeed serverFeed = (ServerFeed)response.body();

        if (serverFeed == null) {
            mEventBus.post(new ListErrorEvent());

        } else {
            mEventBus.post(new ListReceivedEvent(serverFeed));
        }
    }

    @Override
    public void onFailure(Throwable t) {
        mEventBus.post(new ListErrorEvent());
    }
}
