package com.example.gsin.myapplication;

import android.app.Application;

import com.example.gsin.myapplication.module.ApplicationModule;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationModule.setApplication(this);
    }
}