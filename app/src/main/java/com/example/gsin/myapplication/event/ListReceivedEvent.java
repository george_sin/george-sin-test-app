package com.example.gsin.myapplication.event;

import android.util.Log;

import com.example.gsin.myapplication.model.ServerAlbumList;
import com.example.gsin.myapplication.model.ServerEntry;
import com.example.gsin.myapplication.model.ServerFeed;

import java.util.List;

public class ListReceivedEvent {

    private ServerFeed mServerFeed;

    public ListReceivedEvent(ServerFeed feed){
        mServerFeed = feed;
    }

    public ServerFeed getServerFeed(){
        return mServerFeed;
    }
}
